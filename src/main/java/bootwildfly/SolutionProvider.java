package bootwildfly;


import static bootwildfly.Solution.NO_TRADES;

import org.springframework.stereotype.Component;

@Component
public class SolutionProvider {

    public Solution getSolutionForInput(double[] input) {
        if (input.length < 3) {
            return NO_TRADES;
        }
        double[] maxes = new double[input.length];
        Solution currentSolution = NO_TRADES;
        maxes[input.length - 1] = input[input.length - 1];
        maxes[input.length - 2] = Math.max(input[input.length - 1], input[input.length - 2]);
        for (int i = input.length - 3; i >= 0; i--) {
            maxes[i] = Math.max(input[i], maxes[i + 1]);
            Solution potentialNewSolution = new Solution(input[i], maxes[i + 2]);
            if (potentialNewSolution.compareTo(currentSolution) > 0) {
                currentSolution = potentialNewSolution;
            }
        }
        return currentSolution;
    }

}
