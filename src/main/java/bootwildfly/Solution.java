package bootwildfly;

import java.util.Objects;

public class Solution implements Comparable<Solution>{

    public final static Solution NO_TRADES = new Solution(-1, -1);

    private double payOff;

    private double min;

    private double max;

    public double getMin() {
        return min;
    }

    public void setMin(float min) {
        this.min = min;
    }

    public double getMax() {
        return max;
    }

    public void setMax(float max) {
        this.max = max;
    }

    public Solution(double min, double max) {
        this.min = min;
        this.max = max;
        this.payOff = max - min;
    }

    @Override
    public String toString() {
        return "Solution{" +
                "min=" + min +
                ", max=" + max +
                '}';
    }

    @Override
    public int compareTo(Solution o) {
        return Double.compare(this.payOff, o.payOff);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Solution solution = (Solution) o;
        return Double.compare(solution.min, min) == 0 &&
                Double.compare(solution.max, max) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(min, max);
    }
}
