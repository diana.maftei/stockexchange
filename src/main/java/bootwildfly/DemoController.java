package bootwildfly;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class DemoController {


    private final DemoService demoService;

    @Autowired
    public DemoController(DemoService demoService) {
        this.demoService = demoService;
    }


    @RequestMapping(value = "stockExchange", method = RequestMethod.POST)
    public Map<String, Output> getStocksToBuyAndSell(@RequestParam("file") MultipartFile file) {

        if (file.isEmpty()) {
            return null;
        }

        return demoService.getCodesFromFileImages(file);
    }
}