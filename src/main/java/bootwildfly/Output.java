package bootwildfly;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Output {

    @JsonIgnore
    private String inputName;

    private String buyPoint;

    private String sellPoint;

    public String getInputName() {
        return inputName;
    }

    public void setInputName(String inputName) {
        this.inputName = inputName;
    }

    public Output(Solution solution) {
        this.buyPoint = String.valueOf(solution.getMin());
        this.sellPoint = String.valueOf(solution.getMax());
    }

    public Output() {
    }


    public String getBuyPoint() {
        return buyPoint;
    }

    public void setBuyPoint(String buyPoint) {
        this.buyPoint = buyPoint;
    }

    public String getSellPoint() {
        return sellPoint;
    }

    public void setSellPoint(String sellPoint) {
        this.sellPoint = sellPoint;
    }

    @Override
    public String toString() {
        return "Output{" +
                "buyPoint=" + buyPoint +
                ", sellPoint=" + sellPoint +
                '}';
    }
}
