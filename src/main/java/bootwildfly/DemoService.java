package bootwildfly;

import static org.slf4j.LoggerFactory.getLogger;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.imageio.ImageIO;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class DemoService {
    private static final Logger LOGGER = getLogger(DemoService.class);

    @Autowired
    private SolutionProvider solutionProvider;

    public Map<String, Output> getCodesFromFileImages(MultipartFile file) {
        Map<String, Output> codesFromImages = new ConcurrentSkipListMap<>();

        try (ZipInputStream zis = new ZipInputStream(file.getInputStream())) {
            ZipEntry entry;
            
            while ((entry = zis.getNextEntry()) != null) {

                byte bytes[] = new byte[(int) file.getSize()]; // create array to read.
                zis.read(bytes); // read bytes in b

                if (entry.getName().endsWith(".png")) {
                    // process image
                    String inputName = entry.getName();
                    Output output = getResultForInput(inputName, bytes);
                    if (output != null) {
                        codesFromImages.put(output.getInputName(), output);
                    }
                }
            }

        } catch (Exception e) {
            LOGGER.error("Something went wrong", e);
        }
        return codesFromImages;
    }

    private Output getResultForInput(String inputName, byte[] bytes) throws IOException {
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        BufferedImage bufferedImage = ImageIO.read(bais);
        LuminanceSource source = new BufferedImageLuminanceSource(bufferedImage);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        Map<DecodeHintType,Boolean> hints = new HashMap<>();
        hints.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);

        try {
            Result result = new MultiFormatReader().decode(bitmap, hints);
            Solution solution = getStockExchangeOutput(result.getText());
            if (solution != null && solution != Solution.NO_TRADES) {
                Output output = new Output(solution);
                output.setInputName(inputName);
                return output;
            }
        } catch (Exception e) {
            LOGGER.error("Invalid input for " + inputName, e);
        }
        return null;
    }

    public Solution getStockExchangeOutput(String input) {

        String[] splitInputs = input.trim().split(" ");
        double[] values = new double[splitInputs.length];

        for(int i =0; i < splitInputs.length; i++) {
            try{
                double value = Double.valueOf(splitInputs[i]);
                if(value < 0) {
                    return null;
                }
                values[i] = value;
            } catch (Exception e) {
                LOGGER.error("Error getting the value from string: " + splitInputs[i], e);
                return null;
            }
        }

        return solutionProvider.getSolutionForInput(values);
    }

    public SolutionProvider getSolutionProvider() {
        return solutionProvider;
    }

    public void setSolutionProvider(SolutionProvider solutionProvider) {
        this.solutionProvider = solutionProvider;
    }
}
